import { Request, Response } from 'express';
import BaseController from './baseController';
import { WebhookService } from '../services/webhookService';
import { RocketChatService } from '../services/rocketChatService';

export class WebhookController extends BaseController {

    public async getHook(req: Request, res: Response) {
        console.log(process.env)
    }

    public async postHook(req: Request, res: Response) {
        try {
            const result = await WebhookService.processHookRequest(req.body);
            await RocketChatService.sendMessage(result);
            res.json({
                data: result
            });
        } catch (ex) {
            console.log(ex)
            res.status(ex.response.status).json({
                data: ex.response.data
            });
        }
    }
}