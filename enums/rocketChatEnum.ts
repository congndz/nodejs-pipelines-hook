export enum COLOR_MAP {
    INPROGRESS = '#007bff',
    SUCCESSFUL = '#2ecc71',
    FAILED = '#e74c3c'
}