export default abstract class BaseService {
    public static instance;

    public static getInstance() {
        if (!this.instance) {
            this.instance = Object.create(this.prototype);
        }
        return this.instance;
    }
}