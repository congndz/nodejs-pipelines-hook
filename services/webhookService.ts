import BaseService from "./baseService";
import { COLOR_MAP } from "../enums/rocketChatEnum";
import { BUILD_STATE } from "../enums/bitbucketEnum";

export class WebhookService extends BaseService {
    public static processHookRequest(req) {
        const commitStatus = req.commit_status;
        if(!commitStatus) {
            return [];
        }
        const state = commitStatus.state;
        return {
            text: state === BUILD_STATE.INPROGRESS
                ? `Started: [ ${commitStatus.refname.toUpperCase()} ] - Pipeline: (${commitStatus.url}) in *${commitStatus.repository.name.toUpperCase()}*`
                : `Changes: [ ${commitStatus.refname.toUpperCase()} ] - Pipeline: (${commitStatus.url}) in *${commitStatus.repository.name.toUpperCase()}*`,
            attachments: state === BUILD_STATE.INPROGRESS
            ? []
            : [{
                color: COLOR_MAP[commitStatus.state],
                fields: [
                    {
                        title: 'Repository',
                        value: commitStatus.repository.full_name
                    },
                    {
                        title: 'State',
                        value: commitStatus.state
                    },
                    {
                        title: 'Commit',
                        value: `${commitStatus.commit.hash.substr(0, 7)} - ${commitStatus.commit.message} - ${commitStatus.commit.author.raw}`
                    }
                ]
            }]
        };
    }
}