import BaseService from "./baseService";
import axios from "../lib/axios";
import {
    merge,
} from 'lodash'

require('dotenv').config();

const axiosInstance: any = axios
axiosInstance.setConfigure({
    baseURL: process.env.ROCKET_CHAT_URL,
    headers: {
        common: {
            'Content-Type': 'application/json',
            'X-Auth-Token': process.env.ROCKET_CHAT_TOKEN,
            'X-User-Id': process.env.ROCKET_CHAT_USER_ID,
        }
    }
})

export class RocketChatService extends BaseService {
    public static sendMessage(payload) {
        const params = merge({
            channel: process.env.ROCKET_CHAT_CHANNEL
        }, payload);
        return axiosInstance.post('/api/v1/chat.postMessage', params);
    }
}