export interface AxiosInstanceInterface {
    baseURL: string
    headers?: object
    rest?: any
}