import { Request, Response, NextFunction } from "express";
import { WebhookController } from "../controllers/webhookController";

export class Routes {

    public webhookController: WebhookController = new WebhookController()

    public routes(app): void {
        app.route('/webhook')
            .get((req: Request, res: Response, next: NextFunction) => {
                console.log('Middleware')
                console.log(req.query)
                next();
            }, this.webhookController.getHook)
            .post(this.webhookController.postHook);
    }
}
